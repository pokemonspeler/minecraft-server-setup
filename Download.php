#!/bin/php

<?php
$version_data_url = "https://launchermeta.mojang.com/mc/game/version_manifest.json";
$version_data = json_decode(file_get_contents($version_data_url), true);
$version_latest = $version_data["latest"];
$version_latest_data = [];

for ($i = 0; $i < count($version_data["versions"]); $i++) {
    $item = $version_data["versions"][$i];

    if ($item["id"] == $version_latest["release"]) {
        get_server_file($item, "release");
        break;
    }
}

for ($i = 0; $i < count($version_data["versions"]); $i++) {
    $item = $version_data["versions"][$i];

    if ($item["id"] == $version_latest["snapshot"]) {
        get_server_file($item, "snapshot");
        break;
    }
}

function get_server_file($version_object, $type)
{
    $version_url = $version_object["url"];
    $version_data = $version_data = json_decode(file_get_contents($version_url), true);
    safe_server_file(
        $version_data["downloads"]["server"]["url"],
        $version_data["downloads"]["server"]["sha1"],
        $type,
        $version_object["id"]
    );
}

function safe_server_file($download_url, $sha1, $type, $server_version)
{
    $dirname = "./versions/" . $type . "/" . $server_version;
    $filename = $dirname . "/" . "server." . $server_version . ".jar";

    if (!file_exists($dirname)) {
        $mask = umask(0); // set default perms to 0 temporarily
        mkdir($dirname, 0755, true);
        umask($mask);
    }

    if (file_exists($filename)) {
        return;
    }
    $file = file_get_contents($download_url);
    if($sha1 == sha1($file)){
        if (file_put_contents($filename, $file)){
            echo "File downloaded successfully. \n";
        }
        else {
            echo "File downloading failed. \n";
        }
    }
    else{
        echo "File hash of ". $type." (".$server_version.") is invalid, file not saved. \n";
    }




}

